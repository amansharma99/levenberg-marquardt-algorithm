import sys
import numpy as np

"""
def initialize_network(inputs,outputs):
    weights = np.random.rand(outputs,inputs)
    return weights

def sigmoid(x):
    return 1/(1 + np.exp(-x))

def forward(w,x):
"""  

def sigmoid(x):
    return 1/(1 + np.exp(-x))

def sigmoid_der(x):
    return sigmoid(x)*(1-sigmoid(x))

def jacobian(x,w):
    J = np.zeros((len(x),len(w)))
    for i in range(len(x)):
        for j in range(len(w)):
            XW = np.dot(x[i],w)
            z = sigmoid_der(XW)*x[i][j]
            J[i][j] = z
    return J

def main():
    x = np.array([[0,1,0],[0,0,1],[1,0,0],[1,1,0],[1,1,1]])
    y = np.array([1,0,0,1,1])
    
    w = np.random.rand(3,1)
    b = np.random.rand()
    lr = 0.01
    for i in range(10):
        XW = np.dot(x,w)        
        z = sigmoid(XW)
        print(XW.shape)
        y = y.reshape(len(y),1)
        error = z - y
        print(error.sum())
        if(error.sum() <= 0.00001):
            break
        J = jacobian(x,w)
        H = np.dot(J.T,J)
        grad = np.dot(J.T,error)
        I = np.identity(H.shape[0])
        diag = np.zeros((H.shape[0],H.shape[0]))
        for i in range(H.shape[0]):
            diag[i][i] = H[i][i]
        #inv = np.linalg.inv(H + lr*(np.identity(H.shape[0])))
        inv = np.linalg.inv(H + lr*(diag))
        delta = np.dot(inv,grad)
        w -= delta
        XW_new = np.dot(x,w) 
        z_new = sigmoid(XW_new)
        new_error = z_new - y
        if(new_error.sum() <= error.sum()):
            lr /= 10
        else:
            lr *= 10
            w += delta
            
    
    x1 = np.array([1,1,0])
    XW1 = np.dot(x1,w)
    z1 = sigmoid(XW1)
    print(z1)
    
    return 0;
    
    
    """
    w = np.random.rand(3,1)
    b = np.random.rand()
    lr = 0.1
    print('weights: ',w,'\nbias: ',b)
    for i in range(10):
        XW = np.dot(x,w) + b
        print('\nXW: ',XW)
        z = sigmoid(XW)
        print('\nz: ',z)
        error = z - y
        print('\n',error.sum())
        grad = sigmoid_der(z)
        print('\ngrad: ',grad)
        H = sum(np.dot(grad,grad.T))
        print('\nH: ',H)
        print('\nH + lr*I: ',np.linalg.inv(H+lr*(np.identity(H.shape[0]))))
        print('delta_weights:', np.dot(np.linalg.inv(H + lr*(np.identity(H.shape[0]))),grad))
        w -= np.dot(np.linalg.inv(H + lr*(np.identity(H.shape[0]))),grad)
    return 0
"""
    

if __name__=='__main__':
    sys.exit(main())