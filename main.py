#!/usr/bin/env python3


import sys
from datetime import datetime

import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler as SC
import sklearn.metrics as metrics

import LevenbergMarquardt.NN as LM


def main(argv):
    if len(argv) > 1:
        seed = int(argv[1])
    else:
        seed = None

    #### DATA PREPROCESSING ####

    input_attr = ['age', 'sex', 'cp', 'trestbps', 'chol', 'fbs', 'restecg',
                  'thalach', 'exang', 'oldpeak', 'slope','ca','thal']
    
    df = pd.read_csv('Data/heart.csv')
    X = df[input_attr]
    y = df['target']

    X_train, X_test, y_train, y_test = \
        train_test_split(X, y, test_size=0.20, random_state=0)
    sc = SC()
    X_train = sc.fit_transform(X_train)
    X_test = sc.fit_transform(X_test)

    X_train, y_train, X_test, y_test = np.array(X_train),\
        np.array(y_train), np.array(X_test), np.array(y_test)
    y_train = np.reshape(y_train, (len(y_train), 1))
    y_test = np.reshape(y_test, (len(y_test), 1))

    print(X_train.shape, y_train.shape)
    print(X_test.shape, y_test.shape)

    #################################################################

    #### DEFINING THE NEURAL NETWORK ####

    layers = (
        LM.FullyConnectedLayer(13, 6, False, seed=seed),
        LM.FullyConnectedLayer(6, 2, False)
    )

    model = LM.NN(layers, trainType='lm', lr=1)

    startTime = datetime.now()
    model.fit(X_train, y_train, epochs=10)
    print('Train execution time = ', datetime.now() - startTime)

    #################################################################

    #### TESTING ####

    print('Testing...')
    startTime = datetime.now()
    y_pred = model.predict(X_test)
    print('Test execution time = ', datetime.now() - startTime)

    y_pred_labels = list(y_pred > 0.5)

    for i in range(len(y_pred_labels)):
        if int(y_pred_labels[i]) == 1:
            y_pred_labels[i] = 1
        else:
            y_pred_labels[i] = 0


    cm = metrics.confusion_matrix(y_test, y_pred_labels)
    print("\n")
    print("Confusion Matrix : ")
    print(cm)
    print("Accuracy is: ",(cm[0][0]+cm[1][1])*100/(cm[0][0]+cm[1][1]+cm[0][1]+cm[1][0]),"%")

    #################################################################

    return 0


if __name__ == '__main__':
    sys.exit(main(sys.argv))
