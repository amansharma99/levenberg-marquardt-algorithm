# Author: Neel Krishna


import sys
import numpy as np


l = []
def generateColumns(start, end):
    # for i in range(start, end+1):
        # l.extend([str(i)+'X', str(i)+'Y'])
    l = ['age','male', 'female','tb','db','alkphos','sgpt',
         'sgot','tp','alb','ag','truth_value']
    return l

eyes = generateColumns(1, 12)

# reading in the csv as a dataframe
import pandas as pd
df = pd.read_csv('Data/dataset.csv')

# selecting the features and target
X = df[eyes]
y = df['truth_value']

from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = \
    train_test_split(X, y, test_size=0.20, random_state=42)

# Data Normalization
from sklearn.preprocessing import StandardScaler as SC
sc = SC()
X_train = sc.fit_transform(X_train)
X_test = sc.fit_transform(X_test)

import numpy as np
X_train, y_train, X_test, y_test = np.array(X_train), np.array(y_train), np.array(X_test), np.array(y_test)
y_train = np.reshape(y_train, (len(y_train), 1))
y_test = np.reshape(y_test, (len(y_test), 1))


#### LEVENBERG MARQUARDT ALGORITHM ####

import LM

nn_structrue = [X_train.shape[1], 8, y_train.shape[1]]
nn = LM.NN(nn_structrue, seed=int(sys.argv[1]))

print(X_train.shape, y_train.shape)
print(X_test.shape, y_test.shape)
print('Training...')

from datetime import datetime

startTime = datetime.now()
nn.global_search(X_train, y_train)
nn.train_lm(X_train, y_train, epochs=7)
print('Train execution time = ', datetime.now() - startTime)

#######################################

# using the learned weights to predict the target
print('Testing...')
startTime = datetime.now()
y_pred = nn.predict(X_test)
print('Test execution time = ', datetime.now() - startTime)

# setting a confidence threshhold of 0.9
y_pred_labels = list(y_pred > 0.9)

for i in range(len(y_pred_labels)):
    if int(y_pred_labels[i]) == 1 : y_pred_labels[i] = 1
    else : y_pred_labels[i] = 0

# plotting a confusion matrix
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test, y_pred_labels)
print("\n")
print("Confusion Matrix : ")
print(cm)

# creating a dataframe to show results
# df_results = pd.DataFrame()
# df_results['Actual label'] = y_test
# df_results['Predicted value'] = y_pred
# df_results['Predicted label'] = y_pred_labels
# df_results.to_csv(r'C:\Users\91974\Desktop\EYE\Results_new.csv')

# printing execution time of script
