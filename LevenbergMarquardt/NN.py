#!/usr/bin/env python3


import numpy as np
import sklearn.metrics as metrics


def oneHotEncode(x, n):
    ohe = np.zeros(n)
    ohe[x] = 1
    return ohe


def __sigmoid(x):
    return 1 / (1 + np.exp(-x))


def arrayProduct(arr):
    prod = 1
    for i in range(len(arr)):
        prod *= arr[i]
    return prod


def mse(pred, target):
    pred_temp = pred.reshape(arrayProduct(pred.shape))
    target_temp = target.reshape(arrayProduct(target.shape))
    if len(pred_temp) != len(target_temp):
        err = 'Loss cannot be computed between pred.shape=' + \
              str(pred.shape) + ' and target.shape=' + \
              str(target.shape)
        raise ValueError(err)
    err = 0
    for i in range(len(pred_temp)):
        err += (pred_temp[i] - target_temp[i]) ** 2
    return err


class MSELoss(object):

    def __init__(self):
        pass

    def __call__(self, pred, target):
        return mse(pred, target)

    def differentiate(self, pred, target):
        return 2 * (pred - target)


def sigmoid(x):
    flattened = x.reshape(arrayProduct(x.shape))
    sig = np.zeros(shape=flattened.shape)
    for i in range(len(sig)):
        sig[i] = __sigmoid(flattened[i])
    sig = sig.reshape(x.shape)
    return sig


class Sigmoid(object):

    def __init__(self):
        pass

    def __call__(self, x):
        return sigmoid(x)

    def activeDifferentiate(self, x):
        temp = self(x)
        return temp * (np.ones(x.shape) - temp)

    def passiveDifferentiate(self, y):
        return y * (np.ones(y.shape) - y)


def getRandomNDArray(shape, seed=None):
    if seed is not None:
        np.random.seed(seed)
    return np.random.rand(arrayProduct(shape)).reshape(shape)


class FullyConnectedLayer(object):

    def __init__(self, inputs, outputs,
                 bias=True, activation=Sigmoid(), seed=None):
        self.weights = getRandomNDArray((outputs, inputs), seed)
        self.n_inputs = inputs
        self.n_outputs = outputs
        if bias:
            self.bias = getRandomNDArray((outputs,))
        else:
            self.bias = None
        self.activation = activation

    def forward(self, inputs):
        pre_activation = np.matmul(self.weights, inputs)
        if self.bias is not None:
            pre_activation += self.bias
        return self.activation(pre_activation)

    def reinitialize(self, seed=None):
        self.weights = getRandomNDArray((self.n_outputs,
                                         self.n_inputs),
                                        seed)
        if self.bias is not None:
            self.bias = getRandomNDArray((self.n_outputs,), seed)

    def addDeltas(self, weight_deltas, bias_deltas):
        self.weights += weight_deltas
        if self.bias is not None and bias_deltas is not None:
            self.bias += bias_deltas
            
    def jacobian(self, inputs):
        XW = np.matmul(inputs,self.weights.T)
        if self.bias is not None:
            XW += self.bias
        return self.activation(XW)*(1-self.activation(XW))


class NN(object):

    def __init__(self, layers,
                 lossfunc=MSELoss(), trainType='lm', lr=0.1,
                 momentum=0.1):
        self.layers = layers
        self.lossfunc = lossfunc
        # trainType can be 'lm' or 'gd'
        validtrains = ('lm', 'gd')
        if trainType not in validtrains:
            raise ValueError('trainType not in ' + str(validtrains))
        self.trainType = trainType
        # learning rate
        self.lr = lr
        self.momentum = momentum
        self.n_layers = len(layers)
        self.dims_array = [layers[0].n_inputs] +\
                          [layers[i].n_outputs
                           for i in range(self.n_layers)]
        self.__prev_deltas_weights = [np.zeros((self.dims_array[i + 1],
                                     self.dims_array[i]))
                                     for i in range(self.n_layers)]
        self.__prev_deltas_biases = [np.zeros(self.dims_array[i + 1])
                                     if self.layers[i].bias is not None
                                     else None
                                         for i in range(self.n_layers)]

    def forward(self, x):
        intermediate = x
        for i in range(self.n_layers):
            intermediate = self.layers[i].forward(intermediate)
        return intermediate

    def __getIntermediates(self, x):
        intermediates = [x.copy()] +\
                        [None for i in range(self.n_layers)]
        intermediate = x
        for i in range(self.n_layers):
            intermediate = self.layers[i].forward(intermediate)
            intermediates[i + 1] = intermediate.copy()
        return intermediates
    
    def __getJacobian(self, x):
        intermediates = [x.copy()] +\
                        [None for i in range(self.n_layers)]
        intermediate = x
        for i in range(self.n_layers):
            intermediate = self.layers[i].jacobian(intermediate)
            intermediates[i + 1] = intermediate.copy()
        return intermediates

    def __predict(self, x):
        outputs = self.forward(x)
        # argmax index of outputs
        pred_class_num = 0
        for i in range(1, len(outputs)):
            if outputs[i] > outputs[pred_class_num]:
                pred_class_num = i
        return pred_class_num

    def predictSinglePoint(self, x):
        return self.__predict(x)

    def predict(self, inputs):
        outputs = np.zeros((len(inputs),))
        for i in range(len(inputs)):
            outputs[i] = self.__predict(inputs[i])
        return outputs

    def getLoss(self, inputs, target):
        outputs = self.forward(inputs)
        return self.lossfunc(target, outputs)
        

    def getDeltas(self, intermediates, target):
        outputs = intermediates[self.n_layers]
        deltas_weights = [np.zeros((self.dims_array[i + 1],
                                    self.dims_array[i]))
                          for i in range(self.n_layers)]
        deltas_biases = [np.zeros(self.dims_array[i + 1])
                         if self.layers[i].bias is not None
                         else None
                             for i in range(self.n_layers)]
        if self.trainType == 'lm':
            loss_diff = self.lossfunc.differentiate(outputs,
                                                    target)
            loss_diff = loss_diff.reshape(loss_diff.shape[0],1) 
            jacobian = [intermediates[i].reshape(intermediates[i].shape[0],1)
                        for i in range(self.n_layers)]
            delta = [None for i in range(self.n_layers)]
            
            for i in range(self.n_layers-1,-1,-1):
                hessian = np.dot(jacobian[i].T,jacobian[i])
                inverse = np.linalg.inv(hessian +
                                    self.lr*(np.identity(hessian.shape[0])))
                g_dash = self.layers[i].activation.\
                             passiveDifferentiate(intermediates[i+1])
                g_dash = g_dash.reshape(g_dash.shape[0],1)
                if(i == self.n_layers-1):
                    delta[i] = loss_diff
                else:
                    delta[i] = np.multiply(np.dot(self.layers[i+1].weights.T,
                         delta[i+1]),g_dash)
                gradient = np.dot(delta[i],jacobian[i].T)
                
                deltas_weights[i] = np.multiply(inverse,gradient)
                
            return deltas_weights
 
        elif self.trainType == 'gd':
            # Gradient Descent
            loss_diff = self.lossfunc.differentiate(outputs,
                                                    target)
            loss_diff = loss_diff.reshape(loss_diff.shape)
            iter_const = loss_diff.copy()
            for i in range(len(deltas_weights) - 1, -1, -1):
                relevant_inp = intermediates[i]
                relevant_delta = deltas_weights[i], deltas_biases[i]
                activ_diff = self.layers[i].activation.\
                             passiveDifferentiate(intermediates[i +1])
                iter_const *= activ_diff
                for r in range(relevant_delta[0].shape[0]):
                    for c in range(relevant_delta[0].shape[1]):
                        delta = iter_const[r] * relevant_inp[c]
                        relevant_delta[0][r][c] = delta
                    if relevant_delta[1] is not None:
                        delta = iter_const[r]
                        relevant_delta[1][r] = delta
                sum_grads = sum(relevant_delta[0])
                sum_grads = sum_grads.reshape(sum_grads.shape + (1,))
                tmp_iter = iter_const.reshape(iter_const.shape + (1,))
                iter_const = np.matmul(sum_grads,
                                       tmp_iter.transpose()).transpose()
                iter_const = sum(iter_const)
        for i in range(self.n_layers):
            deltas_weights[i] += self.momentum * \
                                 self.__prev_deltas_weights[i]
            deltas_biases[i] += self.momentum * \
                                self.__prev_deltas_biases[i]
        self.__prev_deltas_biases, self.__prev_deltas_biases = \
            deltas_weights, deltas_biases
        return deltas_weights, deltas_biases

    def backward(self, inputs, targets):
        if len(inputs) != len(targets):
            err = 'len(inputs) != len(targets); ' +\
                  str(len(inputs)) + ' != ' + str(len(targets))
            raise ValueError(err)
        deltas_weights = [np.zeros((self.dims_array[i + 1],
                                    self.dims_array[i]))
                          for i in range(self.n_layers)]
        error = [None for i in range(len(inputs))]
        for j in range(len(inputs)):
            x = inputs[j]
            intermediates = self.__getIntermediates(x)
            outputs = intermediates[self.n_layers]
            targ = oneHotEncode(targets[j], self.dims_array[-1])
            loss_diff = self.lossfunc.differentiate(outputs,
                                                    targ)
            loss_diff = loss_diff.reshape(loss_diff.shape)
            error[j] = loss_diff.sum()
            if (j == 0):
                continue
            else:
                if (error[j] <= error[j-1]):
                    if(self.lr > 0.00001):
                        self.lr /= 10
                    else:
                        continue
                elif (error[j] >= error[j-1]):
                    if(self.lr < 10000):
                        for i in range(len(deltas_weights)):
                            self.layers[i].weights += deltas_weights[i]
                        self.lr *= 10
                        j -= 1
                    else:
                        continue
            deltas_weights = self.getDeltas(intermediates, targ)
            for i in range(len(deltas_weights)):
                self.layers[i].weights -= deltas_weights[i]

    def fit(self, inputs, targets,
            epochs=10, feedback_print=True):
        if len(inputs) != len(targets):
            err = 'len(inputs) != len(targets); ' +\
                  str(len(inputs)) + ' != ' + str(len(targets))
            raise ValueError(err)
        if feedback_print:
            print('Training...')
        for ep in range(epochs):
            if feedback_print:
                print('Epoch', ep, 'running...')
                print('Epoch train accuracy:', end=' ')
            self.backward(inputs, targets)
            if feedback_print:
                outputs = self.predict(inputs)
                print(metrics.accuracy_score(targets, outputs))
